package com.kang.msmall.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsmallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsmallMemberApplication.class, args);
    }

}
