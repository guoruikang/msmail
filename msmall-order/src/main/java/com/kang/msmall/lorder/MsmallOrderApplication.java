package com.kang.msmall.lorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsmallOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsmallOrderApplication.class, args);
    }

}
