package com.kang.msmail.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsmailProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsmailProductApplication.class, args);
    }

}
