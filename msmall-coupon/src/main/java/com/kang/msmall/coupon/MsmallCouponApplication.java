package com.kang.msmall.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsmallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsmallCouponApplication.class, args);
    }

}
